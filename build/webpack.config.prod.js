const path = require('path');
const glob = require('glob-all');
const webpack = require('webpack');
const { merge } = require('webpack-merge');

const PurgecssPlugin = require('purgecss-webpack-plugin');

const baseWebpackConfig = require('./webpack.config');

const baseDir = path.resolve(__dirname, '..');

module.exports = merge(baseWebpackConfig, {
    mode: 'production',
    devtool: false,
    plugins: [
        new webpack.DefinePlugin({
            NODE_ENV: JSON.stringify('production'),
        }),
        new PurgecssPlugin({
            paths: glob.sync([
                path.join(baseDir, '_sample/**/*.html'),
                path.join(baseDir, '_includes/**/*.html'),
                path.join(baseDir, '_layouts/**/*.html'),
            ]),
        }),
    ],
});
