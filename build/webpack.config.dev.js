const path = require('path');
const webpack = require('webpack');
const {merge} = require('webpack-merge');

const baseWebpackConfig = require('./webpack.config');

const baseDir = path.resolve(__dirname, '..');

module.exports = merge(baseWebpackConfig, {
    mode: 'development',
    devtool: 'inline-source-map',
    // output: {
        // path: path.resolve(baseDir, 'assets'),
    // },
    devServer: {
        port: 8080,
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        hot: true,
        historyApiFallback: true,
    },
    plugins: [
    ],
});
