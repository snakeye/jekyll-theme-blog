# blog-theme

```bash
docker build -f _docker/Dockerfile -t jekyll-theme-blog .

docker run -it -v (pwd):/opt/src -p 4000:4000 jekyll-theme-blog
```
