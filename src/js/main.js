import '../css/style.scss';

function initNavbarBurgers() {
    // Get all "navbar-burger" elements
    const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

    // Check if there are any navbar burgers
    if ($navbarBurgers.length > 0) {

        // Add a click event on each of them
        $navbarBurgers.forEach(el => {
            el.addEventListener('click', () => {

                // Get the target from the "data-target" attribute
                const target = el.dataset.target;
                const $target = document.getElementById(target);

                // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
                el.classList.toggle('is-active');
                $target.classList.toggle('is-active');
            });
        });
    }
}

function loadGoogleFonts() {
    var wf = document.createElement('script');
    wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
}

function loadAddThis() {
    if(!!window.addthis_pub_id) {
        const el = document.createElement('script');
        el.src = `//s7.addthis.com/js/300/addthis_widget.js#pubid=${ window.addthis_pub_id }`;
        (document.head || document.body).appendChild(el);
    }
}

function loadDisqus() {
    const disqusEl = document.getElementById('disqus');

    if(!!disqusEl && "IntersectionObserver" in window) {
        let disqusObserver = new IntersectionObserver(function(entries, observer) {
            entries.forEach(function(entry) {
              if (entry.isIntersecting) {
                const el = document.createElement('script');
                el.src = `https://${ window.disqus_shortname }.disqus.com/embed.js`;
                el.type = 'text/javascript';
                el.setAttribute('data-timestamp', +new Date())
                document.body.appendChild(el);
                disqusObserver.unobserve(entry.target);
              }
            });
          });

          disqusObserver.observe(disqusEl);
    }
}

function loadLazyImages() {
    var lazyImages = [].slice.call(document.querySelectorAll("img.lazy"));

    if ("IntersectionObserver" in window) {
      let lazyImageObserver = new IntersectionObserver(function(entries, observer) {
        entries.forEach(function(entry) {
          if (entry.isIntersecting) {
            let lazyImage = entry.target;
            lazyImageObserver.unobserve(lazyImage);

            lazyImage.src = lazyImage.dataset.src;
            lazyImage.srcset = lazyImage.dataset.srcset;
            lazyImage.onload = function() {
                lazyImage.classList.remove("lazy");
            };
          }
        });
      });

      lazyImages.forEach(function(lazyImage) {
        lazyImageObserver.observe(lazyImage);
      });
    } else {
      // Possibly fall back to a more compatible method here
    }
}

const currencySymbols = {
    'EUR': '€',
    'USD': '$',
    'GBP': '£',
    'JPY': '¥',
    'RUB': '₽',
};

function loadCurrencyConverter() {
    var moneySpans = [].slice.call(document.querySelectorAll("span.money"));

    if(moneySpans.length == 0){
        return;
    }

    fetch('https://europe-west3-blog-tools-fe4ef.cloudfunctions.net/currencyConverter')
        .then(async response => {
            const exchange = await response.json();

            const {currency, rate} = exchange;

            const symbol = currency in currencySymbols ? currencySymbols[currency] : currency;

            moneySpans.forEach(function(moneySpan) {
                const [valueStr, _] = moneySpan.innerText.split(" ");
                const value = parseFloat(valueStr);
                if(isNaN(value)) {
                    return;
                }

                const convertedValue = (value * rate).toFixed(0);

                moneySpan.innerText = `${convertedValue}${symbol}`;
            });
        })
        .catch(e=>{});
}

function init() {
    initNavbarBurgers();
    loadGoogleFonts();
    loadCurrencyConverter();
    loadLazyImages();
    loadDisqus();

    setTimeout(() => {
        loadAddThis();
    }, 1500);
};

if (document.readyState !== 'loading') init();
else document.addEventListener('DOMContentLoaded', () => {
    init();
}, false);
